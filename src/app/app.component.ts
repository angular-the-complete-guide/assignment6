import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f', {static: false}) signupForm :NgForm;
  defaultSubscription = "advanced";
  submitted = false;
  subscriber = {
    email: '',
    password: '',
    subscription: ''
  }

  onSubmit(){
    this.submitted = true;
    this.subscriber.email = this.signupForm.value.email;
    this.subscriber.password = this.signupForm.value.password;
    this.subscriber.subscription = this.signupForm.value.subscription;
  }
}
